$(document).ready(function() { 

  var wow = new WOW({
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       -100,          // distance to the element when triggering the animation (default is 0)
    mobile:       false,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    scrollContainer: null,    // optional scroll container selector, otherwise use window,
    resetAnimation: true,     // reset animation on end (default is true)
    }
  );

  wow.init();

  function delayItems() {
    $('.link-boxes__item, .offers__item, .home__post').each(function(index){
      var delayNumber = index * .1;
      $(this).attr('data-wow-delay', delayNumber + 's').data('delayNumber');
    })
  }

  delayItems();

  //menu toggle
  function menuToggle() {
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
  }

  menuToggle();

 function slider() {
   //owl slider/carousel
    var $owl = $('.slider__track');

    $owl.each(function() {
      $(this).children().length > 1;

        $(this).owlCarousel({
            loop: true,
            items: 1,
            autoplay: true,
            nav: true,
            dots: true, 
            autplaySpeed: 11000,
            autoplayTimeout: 10000,
            smartSpeed: 250,
            smartSpeed: 2200,
            navSpeed: 2200,
            navText : ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
        });
    });
  }

  slider();

  function referencesSlider() {
   //owl slider/carousel
    var $owl = $('.slider__track--references');

    $owl.each(function() {
      $(this).children().length > 1;

        $(this).owlCarousel({
            loop: true,
            items: 5,
            autoplay: true,
            nav: true,
            dots: true, 
            autplaySpeed: 11000,
            autoplayTimeout: 10000,
            smartSpeed: 250,
            smartSpeed: 2200,
            navSpeed: 2200,
            responsive: {
                0:{
                  items: 2
                },
                480:{
                  items: 3
                },
                769:{
                  items: 5
                }
            },
            navText : ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
        });
    });
  }

  referencesSlider();

  //fancybox
  $('[data-fancybox]').fancybox({   
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false 
    } 
  });


  const getUrl = window.location;
  const getHomeUrl = getUrl.protocol + "//" + getUrl.host;

  //
  // Barba.js
  //
  Barba.Pjax.Dom.wrapperId = 'transition__wrapper';
  Barba.Pjax.Dom.containerClass = 'transition__container';
  Barba.Pjax.ignoreClassLink = 'no-ajax';

  var general = Barba.BaseView.extend({
    namespace: 'general',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
      
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
      $('.transition__pace').removeClass('is-active');

    },
    onLeave: function() {
      // A new Transition toward a new page has just started.
      $('.transition__pace').addClass('is-active');
      
    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.

    }
  });

  general.init();

  Barba.Pjax.start();

  Barba.Prefetch.init(); 

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
  /**
  * This function is automatically called as soon the Transition starts
  * this.newContainerLoading is a Promise for the loading of the new container
  * (Barba.js also comes with an handy Promise polyfill!)
  */
  // As soon the loading is finished and the old page is faded out, let's fade the new page
  Promise
  .all([this.newContainerLoading, this.fadeOut()])
  .then(this.fadeIn.bind(this));
  },
  fadeOut: function() {
  /**
  * this.oldContainer is the HTMLElement of the old Container
  */
  //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  return $(this.oldContainer).animate({ opacity: 0 }).promise();
  },
  fadeIn: function() {
    /**
    * this.newContainer is the HTMLElement of the new Container
    * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
    * Please note, newContainer is available just after newContainerLoading is resolved!
    */
    var _this = this;
    var $newContainer = $(this.newContainer);

    $(this.oldContainer).hide();
    $(window).scrollTop(0); // scroll to top here

    $newContainer.css({
      visibility : 'visible',
      opacity : 0,
      "margin-left" : "30px",
    });

    $newContainer.animate({
      opacity: 1,
      "margin-left" : 0,
    }, 400, function() {
      /**
      * Do not forget to call .done() as soon your transition is finished!
      * .done() will automatically remove from the DOM the old Container
      */
      _this.done();

    });
    }

  });
  /**
  * Next step, you have to tell Barba to use the new Transition
  */
  Barba.Pjax.getTransition = function() {
  /**
  * Here you can use your own logic!
  * For example you can use different Transition based on the current page or link...
  */
  return FadeTransition;

  };

  Barba.Dispatcher.on('initStateChange', function() {

    menuToggle();
    $('body').removeClass('is-fixed');

    if (Barba.HistoryManager.history.length <= 1) {
        return;
    }
    if (typeof gtag === 'function') {
        // send statics by Google Analytics(gtag.js)
        gtag('config', 'GTM-5G68MZP', {'page_path': location.pathname, 'use_amp_client_id': true});
    } else {
        // send statics by Google Analytics(analytics.js) or Google Tag Manager
        if (typeof ga === 'function') {
            var trackers = ga.getAll();
            trackers.forEach(function (tracker) {
                ga(tracker.get('name') + '.send', 'pageview', location.pathname, {'useAmpClientId': true});
            });
        }
    }

  });

  /**
  * GET WP body classes
  */
  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );
    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses);

    slider();
    referencesSlider();
    menuToggle();
    delayItems();
  });

  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {
    if ($('body').hasClass('home')) {
      fbFeed();
    }
  });

});