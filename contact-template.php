<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<?php 
  $adress = get_field('adress', 'options'); 
  $phone = get_field('phone', 'options'); 
  $mail = get_field('mail', 'options'); 
?>

<main>

	<?php get_template_part('parts/page', 'header');?>
	
	<section class="contact padding--bottom">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-6">
					<div class="contact__item">
						<h5 class="contact__info">Her finder du Dan tekstiltryk</h5>

						<p>Adresse: <?php echo esc_html($adress); ?><br></p>
						<p>Telefon: <a href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><?php echo esc_html($phone); ?></a><br></p>
						<p>E-mail: <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a></p>
					</div>
				</div>

				<div class="col-sm-6 contact__form">
					<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>
			</div>
		</div>
	</section>

</main>

<?php get_template_part('parts/footer'); ?>
