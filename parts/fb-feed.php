<?php 
/**
* Description: Lionlab fb feed field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$title = get_field('fb_title', 'options');
$name = get_field('fb_name', 'options');
$link = get_field('fb_link', 'options');
?>

<section class="fb-feed">
  <div class="wrap--fluid hpad">

    <div class="center">
      <h2 class="fb-feed__title"><?php echo esc_html($title); ?></h2>
      <a target="_blank" href="<?php echo esc_url($link); ?>" class="fb-feed__profilename"><?php echo esc_html($name); ?></a>
    </div>

    <div class="row">
        <div id="fb-feed"></div>    
    </div>

  </div>
</section>