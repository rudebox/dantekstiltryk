<?php 
/**
* Description: Lionlab intro  field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$title = get_sub_field('title');
$text = get_sub_field('text');
$img = get_sub_field('img');
$tagline = get_sub_field('tagline');
?>

<section class="intro">
  <div class="wrap hpad">
    <div class="flex flex--wrap intro__row">
        <div class="col-sm-6 intro__text blue--bg">
          <h1 class="intro__title"><?php echo $title; ?></h1>
          <?php echo $text; ?>
        </div>

        <div class="col-sm-6 intro__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
          
          <div class="col-sm-5 intro__tagline blue-light--bg">
            <?php echo esc_html($tagline); ?>
          </div>

        </div>
    </div>
  </div>
</section>