<?php 
/**
* Description: Lionlab offers repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$intro_title = get_sub_field('header_intro');
$link = get_sub_field('link');
$link_text = get_sub_field('link_text');

if (have_rows('offers_box') ) :
?>

<section class="offers <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad offers__container">
		<?php if ($intro_title) : ?>
		<h6 class="offers__header--intro center"><?php echo esc_html($intro_title); ?></h6>
		<?php endif; ?>
		<?php if ($title) : ?>
		<h2 class="offers__header center"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<div class="row flex flex--wrap">
			<?php while (have_rows('offers_box') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-6 col-md-3 offers__item">
				<?php if ($icon) : ?> 
				<div class="offers__wrap">
					<img class="offers__img" src="<?php echo esc_url($icon['sizes']['offer']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
				</div>
				<?php endif; ?>
				<h3 class="offers__title"><?php echo esc_html($title); ?></h3> 
				<?php echo esc_html($text); ?>
				<div class="offers__btn">Læs mere</div>
			</a>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>